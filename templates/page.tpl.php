  <div id="page">

    <header id="masthead" class="site-header clearfix">

      <div id="header-text-nav-container">

        <div class="inner-wrap">
          
          <div id="header-text-nav-wrap" class="clearfix">

            <div id="header-left-section">
              
              <div id="header-logo-image">
                
                <?php if ($logo): ?>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                  </a>
                <?php endif; ?>

              </div><!-- /.header-logo-image -->


              <div id="header-text">
                
                <?php if ($site_name || $site_slogan): ?>
                  <div id="name-and-slogan" class="name-and-slogan-container">
                    <?php if ($site_name): ?>
                      <h1 id="site-name">
                        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                      </h1>
                    <?php endif; ?>

                    <?php if ($site_slogan): ?>
                      <div id="site-slogan"><?php print $site_slogan; ?></div>
                    <?php endif; ?>
                  </div> <!-- /#name-and-slogan -->
                <?php endif; ?>

              </div><!-- /.header-text -->

            </div><!-- /.header-left-section -->

            <div id="header-right-section">

              <?php if ($page['header']): ?>
              <div id="header-right-sidebar" class="clearfix">
                <?php print render($page['header']); ?>
              </div>
              <?php endif; ?>

              <nav id="site-navigation" class="main-navigation" role="navigation">
                <h1 class="menu-toggle">Menu</h1>
                <div class="menu-primary-container">
                  <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'nav-menu', 'clearfix')), 'heading' => '')); ?>
                </div>
              </nav><!-- /site-navigation -->

            </div><!-- /.header-right-section -->

          </div><!-- /.header-text-nav-wrap -->

        </div><!-- /.inner-wrap -->

      </div><!-- /#header-text-nav-container -->

      <?php if ($title): ?>
      <div class="header-post-title-container clearfix">
        <div class="inner-wrap">

          <div class="post-title-wrapper">
            <?php print render($title_prefix); ?>
            <h1 class="title" id="header-post-title-class"><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
          </div>

          <?php if ($breadcrumb): ?>
          <div class="breadcrumb">
            <?php print $breadcrumb; ?>
          </div>
          <?php endif; ?>

        </div><!-- /.inner-wrap -->
      </div><!-- /.header-post-title-container clearfix -->
      <?php endif; ?>

    </header> <!-- /#header -->

    <?php if(theme_get_setting('slider_switch') && $is_front): ?>
      <?php include_once(path_to_theme() . '/inc/slider.inc'); ?>
    <?php endif; ?>

    <div class="inner-wrap">
      <?php print $messages; ?>
    </div><!-- /.inner-wrap -->

    <div class="inner-wrap">

      <?php if ($page['call_to_action']): ?>
      <section class="region_call_to_action clearfix">     
        <div class="call-to-action-content-wrapper clearfix">
          <div class="call-to-action-content">
            <?php print render($page['call_to_action']); ?>
          </div>
        </div>
      </section>
      <?php endif; ?>
    </div><!-- /.inner-wrap -->

    <div id="main" class="clearfix">

      <div class="inner-wrap clearfix">

        <div id="primary">
          <div id="content" class="column">
            <div class="section">
              <a id="main-content"></a>
              <?php if ($tabs_rendered = render($tabs)): ?><div class="tabs"><?php print $tabs_rendered; ?></div><?php endif; ?>
              <?php print render($page['help']); ?>
              <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
              <?php print render($page['content']); ?>
              <?php print $feed_icons; ?>
            </div>
          </div> <!-- /.section, /#content -->
        </div><!-- /.primary -->

        <?php if ($page['sidebar_first']): ?>
        <div id="secondary">
          <div id="sidebar-first" class="column sidebar"><div class="section">
            <?php print render($page['sidebar_first']); ?>
          </div></div> <!-- /.section, /#sidebar-first -->
        </div>
        <?php endif; ?>

      </div><!-- /.inner-wrap -->

    </div> <!-- /#main -->

    <footer id="colophon" class="main-footer">
      <div class="footer-blocks-area inner-wrap clearfix">

        <?php if ($page['footer_1']): ?>
        <div class="footer-1 tg-one-fourth tg-column-1">
          <?php print render($page['footer_1']); ?>
        </div><!-- /.footer-1 -->
        <?php endif; ?>

        <?php if ($page['footer_2']): ?>
        <div class="footer-2 tg-one-fourth tg-column-2">
          <?php print render($page['footer_2']); ?>
        </div><!-- /.footer-2 -->
        <?php endif; ?>

        <?php if ($page['footer_3']): ?>
        <div class="footer-3 tg-one-fourth tg-column-1 tg-after-two-blocks-clearfix">
          <?php print render($page['footer_3']); ?>
        </div><!-- /.footer-3 -->
        <?php endif; ?>

        <?php if ($page['footer_4']): ?>
        <div class="footer-4 tg-one-fourth tg-column-1 tg-one-fourth-last">
          <?php print render($page['footer_4']); ?>
        </div><!-- /.footer-4 -->
        <?php endif; ?>

      </div><!-- /.inner-wrap -->

      <div class="footer-socket-wrapper clearfix">
        <div class="inner-wrap">
          <div class="footer-socket-area">

            <?php if ($page['socket_area']): ?>
            <div class="copyright">
              <?php print render($page['socket_area']); ?>
            </div>            
            <?php endif; ?>

              <nav class="small-menu">
                <div class="menu-footer-menu-container">
                  <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix', 'menu')), 'heading' => '')); ?>
                </div>
              </nav>
          </div><!-- /.footer-socket-area -->
        </div><!-- /.inner-wrap -->
      </div><!-- /.footer-socket-wrapper -->

    </footer> <!-- /.section, /#footer -->


  </div> <!-- /#page -->
