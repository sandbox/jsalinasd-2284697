<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>


  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['name']);
      print render($content);
    ?>
  </div>

  <?php if ($display_submitted): ?>
  <footer class="entry-meta-bar clearfix">                
    <div class="entry-meta clearfix">


      <span class="by-author author vcard"><?php print l($node->name, '/user/' . $node->uid, array('attributes' => array('class' => 'url fn n'))); ?></span>

      <span class="date updated"><?php print format_date($created, 'custom', 'M j,  Y'); ?></span>

      <span class="category">Category</span><!-- TODO: check how to add the category -->

      <span class="comments"><?php if ($comment_count == 0) { print t('No Comments'); } else { print $comment_count; } ; ?></span>

    </div><!-- /.entry-meta-bar -->
  </footer>
  <?php endif; ?>

  <!-- TODO: add a theme option to show or not the user picture on the node -->
  <?php //print $user_picture; ?>

  <?php print render($content['comments']); ?>
</article>
