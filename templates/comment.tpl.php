<div class="comments-area">
    
  <article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <header class="comment-meta comment-author vcard clearfix">
      <div class="avatar photo">
        <?php print $picture ?>
      </div>
      <div class="comment-info">
        <div class="comment-author-link"><?php print $author; ?></div>
        <div class="comment-date-time"><?php print format_date($comment->created, 'custom', 'M j, Y \a\t g\:i a'); ?></div>
        <div class="comment-permalink"><?php print $permalink; ?></div>
      </div>
    </header><!-- .comment-meta -->

    <?php print render($title_prefix); ?>
    <h3 class="comment-title" <?php print $title_attributes; ?>><?php print $title ?></h3>
    <?php print render($title_suffix); ?>

    <section class="comment-content comment clearfix" <?php print $content_attributes; ?>>


      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['links']);
        print render($content);
      ?>

      <?php if ($signature): ?>
      <div class="user-signature clearfix">
        <?php print $signature ?>
      </div>

      <?php endif; ?>

      <?php print render($content['links']) ?>
    </section>

  </article><!-- /.comment -->

</div><!-- /.comments-area -->
