<?php
/*
 *  Implementing preprocess breadcrumb to modify the output of the breadcrumb variable
 */
function spacious_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h4 class="breadcrumb-title">' . t('You are here:') . '</h4>';

    $output .= '<ul class="breadcrumb-elements">' . '<li>' . implode(' » ', $breadcrumb) . '</li>' . '</ul>';
    return $output;
  }
}

/*
 *  Implementing preprocess page
 */
function spacious_preprocess_page(&$variables) {


  //JS files are loaded on the footer when the slider setting is on
  if(theme_get_setting('slider_switch')) {
    drupal_add_js(path_to_theme() . '/js/jquery.cycle.all.js', array('scope' => 'footer'));
    drupal_add_js(path_to_theme() . '/js/spacious-slider-setting.js', array('scope' => 'footer'));
  }

  //The amount of slides comes from the theme settings
  if(theme_get_setting('slider_quantity') >= 1) {
    $slider_quantity  = theme_get_setting('slider_quantity');
  }

  //The default value is 5 slides
  else {
    $slider_quantity  = 5;
  }

  //Loading a list of nodes that are from the Article type, are published and have an image.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'article')
    ->propertyCondition('status', 1)
    ->range(0, $slider_quantity)
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
  $result = $query->execute();
  if (isset($result['node'])) {
    $nodes_nids = array_keys($result['node']);
    $loaded_nodes = entity_load('node', $nodes_nids);
  }

  //Passing those nodes in a variable
  $variables['slides'] = $loaded_nodes;

}
