<?php

  /*
   * Slider template
  */

?>

		<section id="featured-slider">

			<div class="slider-cycle">

        <?php foreach($slides as $slide): ?>

        <?php

          //Setting up variables so the code will be easier to read

          $slide_image = $slide->field_image['und'][0]['uri'];

          $slide_title = $slide->title;

          $slide_body = '';

          if(!empty($slide->body['und'][0]['safe_value'])) {
            $slide_body = truncate_utf8($slide->body['und'][0]['safe_value'], 300, $wordsafe = TRUE, $add_ellipsis = TRUE, $min_wordsafe_length = 1);
          } 

        ?>

        <div class="slides">

          <figure>
            <?php print theme_image(array('path' => $slide_image, 'attributes' => array('class' => array('myclass')))); ?>
          </figure>

          <div class="entry-container">

            <div class="entry-description-container">

              <div class="slider-title-head">
                <h2 class="entry-title">
                  <span><?php print $slide_title; ?></span>
                </h2>
              </div><!-- /.slider-title-head -->

              <?php if(!empty($slide_body)): ?>
              <div class="entry-content"><?php print $slide_body; ?></div>
              <?php endif; ?>

            </div><!-- ./entry-description-container -->

            <div class="clearfix"></div>

            <?php print l(t('Read more'), 'node/'. $slide->nid, array('attributes' => array('class' => array('slider-read-more-button')))); ?>

          </div><!-- /.entry-container -->

        </div><!-- /.slides -->
        <?php endforeach ?>

      </div><!-- /.slider-cycle -->

      <!-- Slider navigation -->
      <nav id="controllers" class="clearfix"></nav>

		</section><!-- /#featured-slider -->
