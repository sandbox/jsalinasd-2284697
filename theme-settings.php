<?php  
function spacious_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['slider_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slider options')
  );

  $form['slider_options']['slider_switch'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show slider on the front page'),
    '#default_value' => theme_get_setting('slider_switch'),
  );

  $form['slider_options']['slider_quantity'] = array(
    '#type' => 'select',
    '#title' => t('How many slides should be printed'),
    '#default_value' => theme_get_setting('slider_quantity'),
     '#options' => array(range(0,15)),
  );
}
